using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using Microsoft.Dynamics365.UIAutomation.Api.UCI;
using NLog;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using TestAutomationProject.API;
using TestAutomationProject.Pages;
using TestAutomationProject.Utilities;
using WebDriver = TestAutomationProject.Utilities.WebDriver;

namespace TestAutomationProject.Tests
{
    public class TestBase
    {
        private IWebDriver driver;
        private WebClient client;
        public XrmApp xrmApp;

        protected static string TesterName;
        protected static string User;
        protected static string Password;
        protected static string Allure;
        protected static string RequestUrl;

        private static ExtentReports _extent;
        private static ExtentTest _test;
        private static Random _random;

        // Pages to be added here
        protected LoginPage LoginPage;

        protected PopularPage PopularPage;
        protected ProfilePage ProfilePage;

        //API
        protected APIUtil APIUtil;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly string _page = MethodBase.GetCurrentMethod().DeclaringType?.ToString();

        [OneTimeSetUp]
        protected static void BeforeSuite()
        {
            // Set project working directory
            Environment.CurrentDirectory = Configuration.ProjectPath();
            var screenshots = Configuration.Path(Configuration.Config.Screenshots);
            var allureReports = Configuration.ProjectPath() + Path.DirectorySeparatorChar + "allure-results";
            var extentResultsDirectory = Configuration.Path(Configuration.Config.Extent) + Path.DirectorySeparatorChar + "Results";
            // Delete old Screenshots
            ClearDirectory(screenshots);
            // Delete old Allure Reports
            ClearDirectory(allureReports);
            // Delete old Extent Reports
            DeleteFolder(extentResultsDirectory);
            // Initialize extent reports
            var extentReport = $"{Configuration.Path(Configuration.Config.Extent)}"
                + Path.DirectorySeparatorChar + "Results"
                + Path.DirectorySeparatorChar + "Result_" + DateTime.Now.ToString("ddMMyyyy HHmmss").Trim() + "\\";
            var extentConfig = $"{Configuration.Path(Configuration.Config.Extent)}Extent-Config.xml";
            _extent = new ExtentReports();
            var htmlReporter = new ExtentHtmlReporter(extentReport);
            htmlReporter.Config.Theme = Theme.Dark;
            htmlReporter.LoadConfig(extentConfig);
            _extent.AddSystemInfo("User Name", "The Best Applicant");
            _extent.AttachReporter(htmlReporter);
        }

        [SetUp]
        public void BeforeMethod()
        {
            string url = "";
            string requestUrl = "";
            string configUser = "";
            string configPassword = "";
            string testerName = "";
            string allureResults = "";

            try
            {
                var browser = Configuration.Read(Configuration.Config.Browser);
                url = Configuration.Read(Configuration.Config.Url);
                testerName = Configuration.Read(Configuration.Config.TesterName);
                configUser = Configuration.Read(Configuration.Config.User);
                configPassword = Configuration.Read(Configuration.Config.Password);
                allureResults = Configuration.Read(Configuration.Config.Allure);
                requestUrl = Configuration.Read(Configuration.Config.RequestUrl);
                // Allocate browser to the driver
                //driver = WebDriver.CreateDriver(browser);
                client = new WebClient(WebDriver.Options);
                driver = client.Browser.Driver;
                xrmApp = new XrmApp(client);
            }
            catch (IOException e)
            {
                Logger.Error(e.StackTrace);
            }

            if (!string.IsNullOrEmpty(url))
            {
                driver.Navigate().GoToUrl(url);
            }

            User = configUser;
            Password = configPassword;
            TesterName = testerName; ;
            Allure = allureResults;
            RequestUrl = requestUrl;

            // Start test logging
            Logger.Info($"------------> TEST STARTED [{WebDriver.WindowSize}] <------------");

            // Extent report details
            _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
            _test.AssignAuthor("Keith de Guzman");
            _test.AssignCategory("Buggy Cars Regression Report");

            // Pages to be added here..
            LoginPage = new LoginPage(driver, client, xrmApp);
            ProfilePage = new ProfilePage(driver, client, xrmApp);
            PopularPage = new PopularPage(driver, client, xrmApp);
            APIUtil = new APIUtil();
        }

        [TearDown]
        public void AfterMethod()
        {
            ReportInfo("Test completed");
            if (TestContext.CurrentContext.Result.Outcome.Status != TestStatus.Passed)
            {
                //CaptureScreen(_driver, TestContext.CurrentContext.Test.MethodName);
                ReportFail($"Test FAILED :: {TestContext.CurrentContext.Test.Name + TestContext.CurrentContext.Result.Message}", CaptureScreen(driver, TestContext.CurrentContext.Test.MethodName));
                Logger.Error($"Error message => {TestContext.CurrentContext.Result.Message}");
                Logger.Error("------------> TEST FAILED <------------");
                Console.WriteLine("Test FAILED");
            }
            else
            {
                ReportPass("Test PASSED");
                Logger.Info("------------> TEST PASSED <------------");
                Console.WriteLine("Test PASSED");
            }
            xrmApp.Dispose();
            WebDriver.QuitDriver(driver);
            try
            {
                var processess = Process.GetProcesses();
                foreach (var process in processess)
                {
                    if (process.ProcessName.ToLower().Contains("chromedriver") || process.ProcessName.ToLower().Contains("chrome"))
                    {
                        process.Kill();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [OneTimeTearDown]
        protected static void AfterSuite()
        {
            _extent.Flush();
        }

        private static string CaptureScreen(IWebDriver driver, string name)
        {
            var screenshots = Configuration.Path(Configuration.Config.Screenshots);
            var runName = $"{name}_{DateTime.Now:yyyy-MM-dd-HH_mm_ss}.png";
            var image = ((ITakesScreenshot)driver).GetScreenshot();
            string path = $"{screenshots}{runName}";
            image.SaveAsFile(path);
            return path;
        }

        public static void ClearDirectory(string directory)
        {
            if (Directory.Exists(directory))
            {
                var di = new DirectoryInfo(directory);
                foreach (var file in di.GetFiles())
                {
                    file.Delete();
                }
            }
            else
            {
                Directory.CreateDirectory(directory);
            }
        }

        public static void DeleteFolder(string directory)
        {
            if (Directory.Exists(directory))
            {
                var di = new DirectoryInfo(directory);
                di.Delete(true);
            }
            else
            {
                Directory.CreateDirectory(directory);
            }
        }

        public static string UploadFile(string fileName)
        {
            string file = $"{Configuration.Path(Configuration.Config.Data)}{fileName}";
            return file;
        }

        public static string RandomDate(int startYear, int endYear)
        {
            var startDate = new DateTime(startYear, 1, 1);
            var endDate = new DateTime(endYear, 12, 31);
            var timeSpan = endDate - startDate;
            var randomTest = new Random();
            var newSpan = new TimeSpan(0, randomTest.Next(0, (int)timeSpan.TotalMinutes), 0);
            return (startDate + newSpan).ToString("ddMMyyyy");
        }

        protected void Sleep(int seconds)
        {
            try
            {
                Thread.Sleep((seconds * 1000));
                Logger.Info($" {_page} - ..Wait :: {seconds} seconds");
            }
            catch (ThreadInterruptedException e)
            {
                Logger.Error(e.StackTrace);
            }
        }

        public enum AlphaNumericType
        {
            Lower,
            Upper,
            Digits,
            Random
        }

        public string RandomString(int length)
        {
            _random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        public int GetRandomNumberInRange(int min, int max)
        {
            if (min >= max)
            {
                Logger.Error($" {_page} - ..!!ERROR!! Maximum value must be greater than minimum value");
                throw new ArgumentException("Maximum value must be greater than minimum value");
            }
            else
            {
                Logger.Info($" {_page} - ..Random number generated between {min} (inclusive) and {max} (inclusive)");
                var random = new Random();
                return random.Next(min, max + 1);
            }
        }

        public string GetUniqueName(int length)
        {
            Logger.Info($" {_page} - ..Unique name generated with length: {length}");
            return RandomString(length).ToLower();
        }

        public string GetUniquePassword(int length)
        {
            Logger.Info($" {_page} - ..Unique name generated with length: {length}");
            return RandomString(length).ToLower() + "1!A";
        }

        public string GetUniqueEmail()
        {
            Logger.Info($" {_page} - ..Unique email generated");
            return $"{GetUniqueName(6)}.{GetUniqueName(7)}@{GetUniqueName(5)}.{GetUniqueName(3)}";
        }

        public bool VerifyTextContains(string actualText, string expectedText)
        {
            Logger.Info(
                $" {_page} - ..Verify if text: {actualText} ..Contains text: {expectedText} ..Result is: {actualText.Contains(expectedText)}");
            return actualText.Contains(expectedText);
        }

        public bool VerifyTextMatch(string actualText, string expectedText)
        {
            Logger.Info(
                $" {_page} - ..Verify if text: {actualText} ..Matches text: {expectedText} ..Result is: {actualText.Contains(expectedText)}");
            return actualText.Equals(expectedText);
        }

        public void SetUrl(string url)
        {
            Logger.Info($" {_page} - ..Set url to: {url}");
            driver.Navigate().GoToUrl(url);
        }

        protected static void ReportInfo(string message)
        {
            _test.Log(Status.Info, message);
        }

        protected static void ReportError(string message)
        {
            _test.Log(Status.Error, message);
        }

        private static void ReportPass(string message)
        {
            _test.Log(Status.Pass, message);
        }

        private static void ReportFail(string message, string path)
        {
            _test.Log(Status.Fail, message);
            _test.AddScreenCaptureFromPath(path);
        }

        protected static void TestName(string name)
        {
            Logger.Info($"------------> TEST {name} <------------");
        }
    }
}