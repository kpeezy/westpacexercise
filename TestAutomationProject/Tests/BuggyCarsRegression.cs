using Allure.Commons;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using TestAutomationProject.Data;
using MIQTestAutomationProject.API.Requests;

namespace TestAutomationProject.Tests
{
    [TestFixture]
    [AllureNUnit]
    [AllureDisplayIgnored]
    public class BuggyCarsRegression : TestBase
    {
        public (string, string) RegisterNewUser(
        )
        {
            string usernameVal = GetUniqueEmail();
            string passwordVal = GetUniquePassword(7);
            CreateNewUser buggyCarsAPI = new CreateNewUser
            {
                username = usernameVal,
                firstName = GetUniqueName(5),
                lastName = GetUniqueName(5),
                password = passwordVal,
                confirmPassword = passwordVal,
            };
            var response = APIUtil.GetResponse(RequestUrl, buggyCarsAPI);
            APIUtil.ResponseVerificaton(response);
            return (usernameVal, passwordVal);
        }

        [Test, Sequential, Order(1)]
        [AllureTag("Regression")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureIssue("BuggyCarsRegression E2E")]
        [Description("Modify Profile")]
        public void LoginModifyProfile(
            [ValueSource(typeof(Read), nameof(Read.FirstName))]
            string firstName,
            [ValueSource(typeof(Read), nameof(Read.LastName))]
            string lastName,
            [ValueSource(typeof(Read), nameof(Read.Gender))]
            string gender,
            [ValueSource(typeof(Read), nameof(Read.Age))]
            string age,
            [ValueSource(typeof(Read), nameof(Read.Address))]
            string address,
            [ValueSource(typeof(Read), nameof(Read.Phone))]
            string phone,
            [ValueSource(typeof(Read), nameof(Read.Hobby))]
            string hobby,
            [ValueSource(typeof(Read), nameof(Read.NewPassword))]
            string newPassword
        )
        {
            (string, string) newUser = RegisterNewUser();
            LoginPage.Login(newUser.Item1, newUser.Item2);
            ProfilePage.NavigateToProfile();
            ProfilePage.ModifyProfile(firstName, lastName, gender, age, address, phone, hobby);
            ProfilePage.ModifyPassword(newUser.Item2, Password);
            ProfilePage.VerifyStatus("The profile has been saved successfully");
        }

        [Test, Sequential, Order(2)]
        [AllureTag("Regression")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureIssue("BuggyCarsRegression E2E")]
        [Description("Add Comment on Popular Car")]
        public void AddComment()
        {
            (string, string) newUser = RegisterNewUser();
            LoginPage.Login(newUser.Item1, newUser.Item2);
            PopularPage.NavigateToPopular();
            (string, string) commentDetails = PopularPage.AddComment();
            PopularPage.VerifyComment(commentDetails.Item1, commentDetails.Item2);
        }

        [Test, Sequential, Order(3)]
        [AllureTag("Regression")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureIssue("BuggyCarsRegression E2E")]
        [Description("Logout After updating Profile")]
        public void VerifyLogoutSecurity()
        {
            LoginPage.Login(User, Password);
            ProfilePage.NavigateToProfile();
            ProfilePage.Logout();
            LoginPage.VerifyUrl("https://buggy.justtestit.org/");
        }
    }
}