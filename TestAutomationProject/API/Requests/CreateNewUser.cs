﻿namespace MIQTestAutomationProject.API.Requests
{
    internal class CreateNewUser
    {
        public string username { get; set; }

        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public string confirmPassword { get; set; }
    }
}