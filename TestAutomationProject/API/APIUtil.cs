﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using System;

namespace TestAutomationProject.API
{
    public class APIUtil
    {
        public IRestResponse GetResponse(string url, object requestBody)
        {
            var client = new RestClient(url);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var body = JsonConvert.SerializeObject(requestBody);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine((int)response.StatusCode);
            return response;
        }

        public void ResponseVerificaton(IRestResponse response)
        {
            int statusCode = (int)response.StatusCode;
            Assert.That(statusCode == 201);
        }
    }
}