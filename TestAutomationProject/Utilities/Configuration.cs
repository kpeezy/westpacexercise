using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace TestAutomationProject.Utilities
{
    public static class Configuration
    {
        public enum Config
        {
            Browser, Drivers, Width, Height, Url, RequestUrl, TesterName, User, Password, SecretKey, Screenshots, Logs, Allure, Extent, Data
        }

        public static string Read(Config search)
        {
            var config = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json").Build();

            return config[search.ToString()];
        }

        public static string Path(Config search)
        {
            var pth = System.Reflection.Assembly.GetCallingAssembly().Location;
            var actualPath = pth.Substring(0, pth.LastIndexOf("bin", StringComparison.Ordinal));
            var projectPath = new Uri(actualPath).LocalPath;
            return $"{projectPath}{search}\\";
        }

        public static string ProjectPath()
        {
            var pth = System.Reflection.Assembly.GetCallingAssembly().Location;
            var actualPath = pth.Substring(0, pth.LastIndexOf("bin", StringComparison.Ordinal));
            var projectPath = new Uri(actualPath).LocalPath;
            return $"{projectPath}";
        }

        public static string LogPath()
        {
            var projectPath = AppContext.BaseDirectory;
            return $"{projectPath}\\{Config.Logs}\\";
        }
    }
}