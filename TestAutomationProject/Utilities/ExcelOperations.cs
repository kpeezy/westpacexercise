using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace TestAutomationProject.Utilities
{
    public class ExcelOperations
    {
        private static DataTable ExcelToDataTable(string fileName, string sheetName)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            var stream = File.Open(fileName, FileMode.Open, FileAccess.Read);
            var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            var resultSet = excelReader.AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                {
                    UseHeaderRow = true
                }
            });
            var table = resultSet.Tables;
            var resultTable = table[sheetName];
            stream.Close();
            return resultTable;
        }

        private class DataCollection
        {
            public int RowNumber { get; set; }
            public string ColName { get; set; }
            public string ColValue { get; set; }
        }

        private static string[] _values = new string[GetTotalRowCount()];
        private static readonly List<DataCollection> DataCol = new List<DataCollection>();
        private static int _totalRowCount;

        private static int GetTotalRowCount()
        {
            return _totalRowCount;
        }

        private static void PopulateInCollection(string fileName, string sheetName)
        {
            var table = ExcelToDataTable(fileName, sheetName);
            _totalRowCount = table.Rows.Count;

            DataCol.Clear();
            for (var row = 1; row <= table.Rows.Count; row++)
            {
                for (var col = 0; col < table.Columns.Count; col++)
                {
                    var dtTable = new DataCollection()
                    {
                        RowNumber = row,
                        ColName = table.Columns[col].ColumnName,
                        ColValue = table.Rows[row - 1][col].ToString()
                    };
                    DataCol.Add(dtTable);
                }
            }
        }

        private static string ReadData(int rowNumber, string columnName)
        {
            try
            {
                var data =
                    (from colData in DataCol
                     where colData.ColName == columnName && colData.RowNumber == rowNumber
                     select colData.ColValue).SingleOrDefault();
                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string[] ReadData(string fileName, string sheetName, string columnName)
        {
            PopulateInCollection(fileName, sheetName);
            _values = new string[GetTotalRowCount()];
            for (var i = 1; i <= GetTotalRowCount(); i++)
            {
                var value = ReadData(i, columnName);
                _values[i - 1] = value;
            }
            return _values;
        }
    }
}