using Microsoft.Dynamics365.UIAutomation.Browser;
using OpenQA.Selenium;
using System;

namespace TestAutomationProject.Utilities
{
    public static class WebDriver
    {
        private static readonly string browserType = Configuration.Read(Configuration.Config.Browser);

        public static BrowserOptions Options = new BrowserOptions
        {
            DriversPath = AppDomain.CurrentDomain.BaseDirectory + (Type.Equals("edge", StringComparison.OrdinalIgnoreCase) ? "\\Drivers\\" : ""),
            BrowserType = (BrowserType)Enum.Parse(typeof(BrowserType), browserType),
            PageLoadTimeout = new TimeSpan(0, 3, 0),
            CommandTimeout = new TimeSpan(0, 10, 0),
            PrivateMode = true,
            DisableInfoBars = true,
            FireEvents = false,
            EnableRecording = false,
            Headless = false,
            UserAgent = false,
            StartMaximized = true,
            DefaultThinkTime = 2000,
            UCITestMode = true,
            UCIPerformanceMode = true
        };

        public static void QuitDriver(IWebDriver driver)
        {
            driver.Quit();
        }

        public static string WindowSize;
    }
}