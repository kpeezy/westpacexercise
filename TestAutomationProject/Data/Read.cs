using TestAutomationProject.Utilities;

namespace TestAutomationProject.Data
{
    public static class Read
    {
        // Data file information
        private const string FileName = "RegressionData.xlsx";

        private static readonly string DataFile = $"{Configuration.Path(Configuration.Config.Data)}{FileName}";

        // Setup test parameters
        public static string[] FirstName()
        {
            return ExcelOperations.ReadData(DataFile, "Profile", "FirstName");
        }

        public static string[] LastName()
        {
            return ExcelOperations.ReadData(DataFile, "Profile", "LastName");
        }

        public static string[] Gender()
        {
            return ExcelOperations.ReadData(DataFile, "Profile", "Gender");
        }

        public static string[] Age()
        {
            return ExcelOperations.ReadData(DataFile, "Profile", "Age");
        }

        public static string[] Address()
        {
            return ExcelOperations.ReadData(DataFile, "Profile", "Address");
        }

        public static string[] Phone()
        {
            return ExcelOperations.ReadData(DataFile, "Profile", "Phone");
        }

        public static string[] Hobby()
        {
            return ExcelOperations.ReadData(DataFile, "Profile", "Hobby");
        }

        public static string[] NewPassword()
        {
            return ExcelOperations.ReadData(DataFile, "Profile", "NewPassword");
        }
    }
}