﻿using Microsoft.Dynamics365.UIAutomation.Api.UCI;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;

namespace TestAutomationProject.Pages
{
    public class PopularPage : ActionBase
    {
        // Constructor : Inheritance from PageBase
        public PopularPage(IWebDriver driver, WebClient client, XrmApp xrmapp) : base(driver, client, xrmapp)
        {
        }

        private readonly string _page = MethodBase.GetCurrentMethod().DeclaringType?.ToString();

        // Methods

        public void NavigateToPopular()
        {
            ElementClick(_page, LocatorType.XPath, "//img[@title='Diablo']");
        }

        public (string, string) AddComment()
        {
            string numberOfVotes = GetText(_page, LocatorType.XPath, "//h4//strong");
            string comment = "the current date is: " + DateTime.Now.AddDays(30).ToString("dd/MM/yyyy hh:mm tt");
            SendKeys(_page, LocatorType.Id, "comment", comment);
            ElementClick(_page, LocatorType.XPath, "//button[text()='Vote!']");
            return (numberOfVotes, comment);
        }

        public void VerifyComment(string numberOfVote, string expectedComment)
        {
            IsElementDisplayed(_page, LocatorType.XPath, "//tr/td[text()='" + expectedComment + "']");
            int expectedNumberOfVote = Int32.Parse(numberOfVote) + 1;
            int actualNumberOfVote = Int32.Parse(GetText(_page, LocatorType.XPath, "//h4//strong"));
            Assert.That(expectedNumberOfVote == actualNumberOfVote, "expected number of Votes: " + expectedNumberOfVote + " but was: " + actualNumberOfVote);
        }
    }
}