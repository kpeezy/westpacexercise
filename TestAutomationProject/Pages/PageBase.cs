using Microsoft.Dynamics365.UIAutomation.Api.UCI;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace TestAutomationProject.Pages
{
    public class PageBase
    {
        // Constructor
        protected PageBase(IWebDriver _driver, WebClient _client, XrmApp _xrmApp)
        {
            driver = _driver;
            client = _client;
            xrmApp = _xrmApp;
            Wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
        }

        // Properties
        private IWebDriver driver { get; }

        private XrmApp xrmApp { get; }
        private WebClient client { get; }

        private static WebDriverWait Wait { get; set; }

        public enum LocatorType
        {
            Id,
            Name,
            Css,
            Class,
            Link,
            XPath
        }

        // Methods
        public string GetTitle()
        {
            return driver.Title;
        }

        public string GetUrl()
        {
            string url = driver.Url.ToString();
            return url;
        }

        protected void NavigateToUrl(string method, string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void ErrorHandler(string method, string message, Exception error)
        {
            Logger.Error(message);
            Logger.Error($" {method} -  {error.Message}");
            Logger.Error($" {method} -  {error.GetBaseException()}");
            Assert.Fail(message);
        }

        protected IWebElement GetElement(LocatorType locatorType, string locator)
        {
            IWebElement element;
            switch (locatorType)
            {
                case LocatorType.Id:
                    element = driver.FindElement(By.Id(locator));
                    break;

                case LocatorType.Name:
                    element = driver.FindElement(By.Name(locator));
                    break;

                case LocatorType.Css:
                    element = driver.FindElement(By.CssSelector(locator));
                    break;

                case LocatorType.Class:
                    element = driver.FindElement(By.ClassName(locator));
                    break;

                case LocatorType.Link:
                    element = driver.FindElement(By.LinkText(locator));
                    break;

                case LocatorType.XPath:
                    element = driver.FindElement(By.XPath(locator));
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(locatorType), locatorType, null);
            }

            return element;
        }

        protected static void WaitElement(LocatorType locatorType, string locator)
        {
            switch (locatorType)
            {
                case LocatorType.Id:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Id(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id(locator)));
                    break;

                case LocatorType.Name:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Name(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Name(locator)));
                    break;

                case LocatorType.Css:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector(locator)));
                    break;

                case LocatorType.Class:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.ClassName(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.ClassName(locator)));
                    break;

                case LocatorType.Link:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.LinkText(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText(locator)));
                    break;

                case LocatorType.XPath:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath(locator)));
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(locatorType), locatorType, null);
            }
        }

        protected void WaitElementToBeInvicible(LocatorType locatorType, string locator)
        {
            switch (locatorType)
            {
                case LocatorType.Id:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Id(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id(locator)));
                    break;

                case LocatorType.Name:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Name(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Name(locator)));
                    break;

                case LocatorType.Css:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.CssSelector(locator)));
                    break;

                case LocatorType.Class:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.ClassName(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.ClassName(locator)));
                    break;

                case LocatorType.Link:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.LinkText(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.LinkText(locator)));
                    break;

                case LocatorType.XPath:
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(locator)));
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.XPath(locator)));
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(locatorType), locatorType, null);
            }
        }

        protected void SendKeys(string method, LocatorType locatorType, string locator, string input)
        {
            try
            {
                if (!string.IsNullOrEmpty(input))
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    element.Clear();
                    element.SendKeys(input);
                    Logger.Info(
                        $" {method} - ..Sent data to element with locator: {locator} ..LocatorType: {locatorType} ..Data entered: {input}");
                }
                else
                {
                    Logger.Info(
                        $" {method} - ..NO data sent to element with locator: {locator} ..LocatorType: {locatorType} ..Data entered: {input}");
                }
            }
            catch (Exception e)
            {
                var errorMessage =
                    $" {method} - ..!!ERROR!! NO data sent to element with locator: {locator} ..LocatorType: " +
                    $"{locatorType.ToString()} ..Data entered: {input}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void CMSSendKeys(string method, LocatorType locatorType, string locator, string input)
        {
            try
            {
                if (!string.IsNullOrEmpty(input))
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    element.Click();
                    xrmApp.ThinkTime(300);
                    element.Clear();
                    xrmApp.ThinkTime(300);
                    element.SendKeys(input);
                    Logger.Info(
                        $" {method} - ..Sent data to element with locator: {locator} ..LocatorType: {locatorType} ..Data entered: {input}");
                }
                else
                {
                    Logger.Info(
                        $" {method} - ..NO data sent to element with locator: {locator} ..LocatorType: {locatorType} ..Data entered: {input}");
                }
            }
            catch (Exception e)
            {
                var errorMessage =
                    $" {method} - ..!!ERROR!! NO data sent to element with locator: {locator} ..LocatorType: " +
                    $"{locatorType.ToString()} ..Data entered: {input}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void DoubleClickSendKeys(string method, LocatorType locatorType, string locator, string input)
        {
            try
            {
                if (!string.IsNullOrEmpty(input))
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    element.Click();
                    element.Click();
                    xrmApp.ThinkTime(500);
                    element.SendKeys(input);
                    //element.Clear();
                    Logger.Info(
                        $" {method} - ..Sent data to element with locator: {locator} ..LocatorType: {locatorType} ..Data entered: {input}");
                }
                else
                {
                    Logger.Info(
                        $" {method} - ..NO data sent to element with locator: {locator} ..LocatorType: {locatorType} ..Data entered: {input}");
                }
            }
            catch (Exception e)
            {
                var errorMessage =
                    $" {method} - ..!!ERROR!! NO data sent to element with locator: {locator} ..LocatorType: " +
                    $"{locatorType.ToString()} ..Data entered: {input}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void ClearField(string method, LocatorType locatorType, string locator)
        {
            try
            {
                WaitElement(locatorType, locator);
                var element = GetElement(locatorType, locator);
                Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                element.Clear();
                Logger.Info($" {method} - ..Element Cleared with locator: {locator} ..LocatorType: {locatorType}");
            }
            catch (Exception e)
            {
                var errorMessage =
                    $" {method} - ..!!ERROR!! Element NOT Cleared with locator: {locator} ..LocatorType: {locatorType}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void ElementClick(string method, LocatorType locatorType, string locator)
        {
            try
            {
                WaitElement(locatorType, locator);
                var element = GetElement(locatorType, locator);
                Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                element.Click();
                Logger.Info($" {method} - ..Element Clicked with locator: {locator} ..LocatorType: {locatorType}");
            }
            catch (Exception e)
            {
                var errorMessage =
                    $" {method} - ..!!ERROR!! Element NOT Clicked with locator: {locator} ..LocatorType: {locatorType}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void ElementSelect(string method, LocatorType locatorType, string locator, string select)
        {
            try
            {
                if (!string.IsNullOrEmpty(select))
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    new SelectElement(element).SelectByText(select);
                    Logger.Info($" {method} - ..Element selected with locator: {locator} ..LocatorType: " +
                                $"{locatorType} ..Selection: {select}");
                }
                else
                {
                    Logger.Info($" {method} - ..NO element selected with locator: {locator} ..LocatorType: " +
                                $"{locatorType} ..Selection: {select}");
                }
            }
            catch (Exception e)
            {
                var errorMessage =
                    $" {method} - ..!!ERROR!! NO element selected with locator: {locator} ..LocatorType: " +
                    $"{locatorType} ..Selection: {select}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void ElementCheck(string method, LocatorType locatorType, string locator, string option)
        {
            try
            {
                if (!string.IsNullOrEmpty(option))
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    var check = element.Selected;
                    if (!check && option.ToLower().Equals("y"))
                    {
                        //check
                        element.Click();
                        Logger.Info($" {method} - ..Element checked with locator: " +
                                    $"{locator} ..LocatorType: {locatorType}");
                    }

                    if (check && !"y".Equals(option.ToLower()))
                    {
                        //uncheck
                        element.Click();
                        Logger.Info($" {method} - ..Element unchecked with locator: " +
                                    $"{locator} ..LocatorType: {locatorType}");
                    }
                }
                else
                    Logger.Info($" {method} - ..Element NOT checked or unchecked with locator: " +
                                $"{locator} ..LocatorType: {locatorType}");
            }
            catch (Exception e)
            {
                var errorMessage = $" {method} - ..!!ERROR!! Element NOT checked or unchecked with locator: " +
                                   $"{locator} ..LocatorType: {locatorType}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        private ReadOnlyCollection<IWebElement> GetElementList(LocatorType locatorType, string locator)
        {
            ReadOnlyCollection<IWebElement> elements;
            switch (locatorType)
            {
                case LocatorType.Id:
                    elements = driver.FindElements(By.Id(locator));
                    break;

                case LocatorType.Name:
                    elements = driver.FindElements(By.Name(locator));
                    break;

                case LocatorType.Css:
                    elements = driver.FindElements(By.CssSelector(locator));
                    break;

                case LocatorType.Class:
                    elements = driver.FindElements(By.ClassName(locator));
                    break;

                case LocatorType.Link:
                    elements = driver.FindElements(By.LinkText(locator));
                    break;

                case LocatorType.XPath:
                    elements = driver.FindElements(By.XPath(locator));
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(locatorType), locatorType, null);
            }

            return elements;
        }

        protected int GetElementCount(string method, LocatorType locatorType, string locator)
        {
            try
            {
                var listCount = GetElementList(locatorType, locator).Count;
                Logger.Info($" {method} - ..Element count with locator: {locator} ..LocatorType: " +
                            $"{locatorType} ..Count: {listCount}");
                return listCount;
            }
            catch (Exception e)
            {
                Logger.Info($" {method} - ..!!NOT FOUND!! Element count NOT possible with locator: " +
                            $"{locator} ..LocatorType: {locatorType}");
                Logger.Info($" {method} - {e.Message}");
                return 0;
            }
        }

        protected string GetText(string method, LocatorType locatorType, string locator)
        {
            try
            {
                WaitElement(locatorType, locator);
                var element = GetElement(locatorType, locator);
                var text = element.Text;
                if (string.IsNullOrEmpty(text))
                {
                    text = element.GetAttribute("value");
                }

                if (string.IsNullOrEmpty(text))
                {
                    text = element.GetAttribute("innerText");
                }

                if (!string.IsNullOrEmpty(text))
                {
                    text = text.Trim();
                }

                Logger.Info($" {method} - ..Element get text with locator: {locator} ..LocatorType: " +
                            $"{locatorType} ..Text found: {text}");
                return text;
            }
            catch (Exception e)
            {
                Logger.Info($" {method} - ..!!NOT FOUND!! Element get text NOT possible with locator: " +
                            $"{locator} ..LocatorType: {locatorType}");
                Logger.Info($" {method} - {e.Message}");
                return null;
            }
        }

        protected string GetAttribute(string method, LocatorType locatorType, string locator, string attribute)
        {
            try
            {
                WaitElement(locatorType, locator);
                var element = GetElement(locatorType, locator);
                var text = element.Text;
                if (string.IsNullOrEmpty(text))
                {
                    text = element.GetAttribute(attribute);
                }

                Logger.Info($" {method} - ..Element get attribute with locator: {locator} ..LocatorType: " +
                            $"{locatorType} ..Attribute found: {text}");
                return text;
            }
            catch (Exception e)
            {
                Logger.Info($" {method} - ..!!NOT FOUND!! Element get attribute NOT possible with locator: " +
                            $"{locator} ..LocatorType: {locatorType}");
                Logger.Info($" {method} - {e.Message}");
                return null;
            }
        }

        protected bool WriteProtectedText(string method, LocatorType locatorType, string locator)
        {
            try
            {
                WaitElement(locatorType, locator);
                var element = GetElement(locatorType, locator);
                var text = element.GetAttribute("disabled");
                var result = text.Equals("true", StringComparison.OrdinalIgnoreCase);
                Logger.Info($" {method} - ..Element write protected with locator: " +
                            $"{locator} ..LocatorType: {locatorType} ..Result: {result}");
                return result;
            }
            catch (Exception e)
            {
                Logger.Info(
                    $" {method} - ..!!NOT FOUND!! Unable to see if element is write protected with locator: " +
                    $"{locator} ..LocatorType: {locatorType}");
                Logger.Info($" {method} - {e.Message}");
                return false;
            }
        }

        protected bool IsElementPresent(string method, LocatorType locatorType, string locator)
        {
            try
            {
                WaitElement(locatorType, locator);
                GetElement(locatorType, locator);
                Logger.Info($" {method} - ..Element present with locator: {locator} ..LocatorType: {locatorType}");
                return true;
            }
            catch (Exception e)
            {
                Logger.Info($" {method} - ..Element NOT present with locator: {locator} ..LocatorType: {locatorType}");
                Logger.Info($" {method}  - {e.Message}");
                return false;
            }
        }

        protected bool IsElementDisplayed(string method, LocatorType locatorType, string locator)
        {
            try
            {
                WaitElement(locatorType, locator);
                var element = GetElement(locatorType, locator);
                Logger.Info(element.Displayed
                    ? $" {method} - ..Element displayed with locator: {locator} ..LocatorType: {locatorType}"
                    : $" {method} - ..Element NOT displayed with locator: {locator} ..LocatorType: {locatorType}");

                return element.Displayed;
            }
            catch (Exception e)
            {
                Logger.Info(
                    $" {method} - ..Element NOT displayed with locator: {locator} ..LocatorType: {locatorType}");
                Logger.Info($" {method} - {e.Message}");
                return false;
            }
        }

        protected bool IsElementChecked(string method, LocatorType locatorType, string locator)
        {
            try
            {
                var element = GetElement(locatorType, locator);
                Logger.Info(element.Selected
                    ? $" {method} - ..Element IS checked with locator: {locator} ..LocatorType: {locatorType}"
                    : $" {method} - ..Element NOT checked with locator: {locator} ..LocatorType: {locatorType}");
                return element.Selected;
            }
            catch (Exception e)
            {
                Logger.Info(
                    $" {method} - ..Element NOT checked with locator: {locator} ..LocatorType: {locatorType}");
                Logger.Info($" {method} - {e.Message}");
                return false;
            }
        }

        protected bool ElementPresenceCheck(string method, LocatorType locatorType, string locator)
        {
            try
            {
                var elements = GetElementList(locatorType, locator);
                if (elements.Count > 0)
                {
                    Logger.Info(
                        $" {method} - ..Element presence check with locator: {locator} ..LocatorType: {locatorType}");
                    return true;
                }
                else
                {
                    Logger.Info(
                        $" {method} - ..NO element presence check with locator: {locator} ..LocatorType: {locatorType}");
                    return false;
                }
            }
            catch (Exception e)
            {
                Logger.Info(
                    $" {method} - ..!!ERROR!! NO element presence check with locator: {locator} ..LocatorType: {locatorType}");
                Logger.Info($" {method} - {e.Message}");
                return false;
            }
        }

        protected void WebScroll(string method, string direction)
        {
            try
            {
                var je = (IJavaScriptExecutor)driver;
                if (direction.ToLower().Equals("up"))
                {
                    je.ExecuteScript("window.scrollBy(0,-1000)");
                    Logger.Info($" {method} - ..Window scrolled up");
                }
                else if (direction.ToLower().Equals("down"))
                {
                    je.ExecuteScript("window.scrollBy(0,1000)");
                    Logger.Info($" {method} - ..Window scrolled down");
                }
                else if (direction.ToLower().Equals("right"))
                {
                    je.ExecuteScript("window.scrollBy(1000,0)");
                    Logger.Info($" {method} - ..Window scrolled right");
                }
                else if (direction.ToLower().Equals("left"))
                {
                    je.ExecuteScript("window.scrollBy(-1000,0)");
                    Logger.Info($" {method} - ..Window scrolled left");
                }
            }
            catch (Exception e)
            {
                var errorMessage = $" {method} - ..!!ERROR!! Unable to scroll";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void SwitchFrame(string method, string frame = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(frame))
                {
                    driver.SwitchTo().Frame(frame);
                    Logger.Info($" {method} - ..Switched to frame: {frame}");
                }
                else
                {
                    driver.SwitchTo().DefaultContent();
                }
            }
            catch (Exception e)
            {
                var errorMessage = $" {method} - ..!!ERROR!! Unable to switch to frame: {frame}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void SwitchFrameByLocator(string method, LocatorType locatorType, string locator)
        {
            try
            {
                var element = GetElement(locatorType, locator);
                driver.SwitchTo().Frame(element);
                Logger.Info($" {method} - ..Switched to frame: {element}");
            }
            catch (Exception e)
            {
                var errorMessage = $" {method} - ..!!ERROR!! Unable to switch to frame with {locator} ..LocatorType: {locatorType}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void SwitchWindow(string method)
        {
            try
            {
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Logger.Info($" {method} - ..Switched to window");
            }
            catch (Exception e)
            {
                var errorMessage = $" {method} - ..!!ERROR!! Unable to switch to window";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void AlertResponse(string method, string response)
        {
            try
            {
                Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.AlertIsPresent());
                var alert = driver.SwitchTo().Alert();
                if (response.ToLower().Equals("y"))
                {
                    alert.Accept();
                    Logger.Info($" {method} - ..Alert accepted");
                }
                else
                {
                    alert.Dismiss();
                    Logger.Info($" {method} - ..Alert dismissed");
                }
            }
            catch (Exception e)
            {
                var errorMessage = $" {method} - ..!!ERROR!! Unable to handle alert";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void DragAndDrop(string method, LocatorType fromLocatorType, string fromLocator,
            LocatorType toLocatorType,
            string toLocator)
        {
            try
            {
                var fromElement = GetElement(fromLocatorType, fromLocator);
                var toElement = GetElement(toLocatorType, toLocator);
                var act = new Actions(driver);
                act.DragAndDrop(fromElement, toElement).Build().Perform();
                Logger.Info($" {method} - ..Drag and drop From locator: {fromLocator} ..From locatorType: " +
                            $"{fromLocatorType} .. To locator: {toLocator} .. To locatorType: {toLocatorType}");
            }
            catch (Exception e)
            {
                var errorMessage = $" {method} - ..!!ERROR!! Unable to drag and drop";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void MouseHovering(string method, LocatorType locatorType, string locator)
        {
            try
            {
                WaitElement(locatorType, locator);
                var element = GetElement(locatorType, locator);
                var act = new Actions(driver);
                act.MoveToElement(element).Build().Perform();
                Logger.Info($" {method} - ..Mouse hovering to locator: {locator} ..LocatorType: {locatorType}");
            }
            catch (Exception e)
            {
                var errorMessage = $" {method} - ..!!ERROR!! Unable to hover with mouse";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected static void Sleep(string method, int seconds)
        {
            try
            {
                Thread.Sleep(seconds * 1000);
                Logger.Info($" {method} - ..Wait :: seconds + {seconds}");
            }
            catch (ThreadInterruptedException e)
            {
                Logger.Error(e.StackTrace);
                Assert.Fail(e.Message);
            }
        }

        protected void ElementBackSpace(string method, LocatorType locatorType, string locator, string repeats)
        {
            var n = 1;
            while (n <= int.Parse(repeats))
            {
                try
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    element.SendKeys(Keys.Backspace);
                    Logger.Info(
                        $" {method} - ..Element back space with locator: {locator} ..LocatorType: {locatorType}");
                }
                catch (Exception e)
                {
                    var errorMessage = $" {method} - ..!!ERROR!! Element NOT back spaced with locator: " +
                                       $"{locator} ..LocatorType: {locatorType}";
                    ErrorHandler(method, errorMessage, e);
                }

                n++;
            }
        }

        protected void ElementUpArrow(string method, LocatorType locatorType, string locator, string repeats)
        {
            var n = 1;
            while (n <= int.Parse(repeats))
            {
                try
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    element.SendKeys(Keys.ArrowUp);
                    Logger.Info($" {method} - ..Element up arrow key with locator: {locator} ..LocatorType: " +
                                $"{locatorType}");
                }
                catch (Exception e)
                {
                    var errorMessage =
                        $" {method} - ..!!ERROR!! Element NOT up arrow key with locator: {locator} ..LocatorType: " +
                        $"{locatorType}";
                    ErrorHandler(method, errorMessage, e);
                }

                n++;
            }
        }

        protected void ElementDownArrow(string method, LocatorType locatorType, string locator, string repeats)
        {
            var n = 1;
            while (n <= int.Parse(repeats))
            {
                try
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    element.SendKeys(Keys.ArrowDown);
                    Logger.Info($" {method} - ..Element down arrow key with locator: {locator} ..LocatorType: " +
                                $"{locatorType}");
                }
                catch (Exception e)
                {
                    var errorMessage =
                        $" {method} - ..!!ERROR!! Element NOT down arrow key with locator: {locator} ..LocatorType: " +
                        $"{locatorType}";
                    ErrorHandler(method, errorMessage, e);
                }

                n++;
            }
        }

        protected void ElementLeftArrow(string method, LocatorType locatorType, string locator, string repeats)
        {
            var n = 1;
            while (n <= int.Parse(repeats))
            {
                try
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    element.SendKeys(Keys.ArrowLeft);
                    Logger.Info($" {method} - ..Element left arrow key with locator: {locator} ..LocatorType: " +
                                $"{locatorType}");
                }
                catch (Exception e)
                {
                    var errorMessage =
                        $" {method} - ..!!ERROR!! Element NOT left arrow key with locator: {locator} ..LocatorType: " +
                        $"{locatorType}";
                    ErrorHandler(method, errorMessage, e);
                }

                n++;
            }
        }

        protected void ElementRightArrow(string method, LocatorType locatorType, string locator, string repeats)
        {
            var n = 1;
            while (n <= int.Parse(repeats))
            {
                try
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    element.SendKeys(Keys.ArrowRight);
                    Logger.Info($" {method} - ..Element right arrow key with locator: {locator} ..LocatorType: " +
                                $"{locatorType}");
                }
                catch (Exception e)
                {
                    var errorMessage =
                        $" {method} - ..!!ERROR!! Element NOT right arrow key with locator: {locator} ..LocatorType: " +
                        $"{locatorType}";
                    ErrorHandler(method, errorMessage, e);
                }

                n++;
            }
        }

        protected void ElementDeleteAll(string method, LocatorType locatorType, string locator)
        {
            try
            {
                WaitElement(locatorType, locator);
                var element = GetElement(locatorType, locator);
                Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                element.SendKeys(Keys.Control + "a" + Keys.Delete);
                Logger.Info($" {method} - ..Element delete all with locator: {locator} ..LocatorType: " +
                            $"{locatorType}");
            }
            catch (Exception e)
            {
                var errorMessage = $" {method} - ..!!ERROR!! Element NOT all deleted with locator: " +
                                   $"{locator} ..LocatorType: {locatorType}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void ElementEnter(string method, LocatorType locatorType, string locator)
        {
            try
            {
                WaitElement(locatorType, locator);
                var element = GetElement(locatorType, locator);
                Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                element.SendKeys(Keys.Enter);
                Logger.Info($" {method} - ..Element enter all with locator: {locator} ..LocatorType: " +
                            $"{locatorType}");
            }
            catch (Exception e)
            {
                var errorMessage =
                    $" {method} - ..!!ERROR!! Element NOT all entered with locator: {locator} ..LocatorType: " +
                    $"{locatorType}";
                ErrorHandler(method, errorMessage, e);
            }
        }

        protected void ElementTabKey(string method, LocatorType locatorType, string locator, string repeats)
        {
            var n = 1;
            while (n <= int.Parse(repeats))
            {
                try
                {
                    WaitElement(locatorType, locator);
                    var element = GetElement(locatorType, locator);
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    element.SendKeys(Keys.Tab);
                    Logger.Info($" {method} - ..Element tab key with locator: {locator} ..LocatorType: " +
                                $"{locatorType}");
                }
                catch (Exception e)
                {
                    var errorMessage =
                        $" {method} - ..!!ERROR!! Element NOT tab key with locator: {locator} ..LocatorType: " +
                        $"{locatorType}";
                    ErrorHandler(method, errorMessage, e);
                }

                n++;
            }
        }
    }
}