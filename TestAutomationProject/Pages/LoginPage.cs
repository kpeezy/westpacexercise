﻿using Microsoft.Dynamics365.UIAutomation.Api.UCI;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Reflection;

namespace TestAutomationProject.Pages
{
    public class LoginPage : ActionBase
    {
        // Constructor : Inheritance from PageBase
        public LoginPage(IWebDriver driver, WebClient client, XrmApp xrmapp) : base(driver, client, xrmapp)
        {
        }

        private readonly string _page = MethodBase.GetCurrentMethod().DeclaringType?.ToString();

        // Methods

        public void Login(string emailAddress, string password)
        {
            SendKeys(_page, LocatorType.XPath, "//input[@name='login']", emailAddress);
            SendKeys(_page, LocatorType.XPath, "//input[@name='password']", password);
            ElementClick(_page, LocatorType.XPath, "//button[@type='submit']");
        }

        public void VerifyUrl(string url)
        {
            string expected = url;
            string actual = GetUrl();
            Assert.That(expected == actual, "expected url: " + expected + " but was: " + actual);
        }
    }
}