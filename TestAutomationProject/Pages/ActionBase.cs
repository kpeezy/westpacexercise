﻿using Microsoft.Dynamics365.UIAutomation.Api.UCI;
using OpenQA.Selenium;

namespace TestAutomationProject.Pages
{
    public class ActionBase : PageBase
    {
        // Constructor : Inheritance from PageBase
        protected ActionBase(IWebDriver _driver, WebClient _client, XrmApp _xrmApp) : base(_driver, _client, _xrmApp)
        {
            driver = _driver;
            client = _client;
            xrmApp = _xrmApp;
        }

        // Properties
        private IWebDriver driver { get; }

        private XrmApp xrmApp { get; }
        private WebClient client { get; }

        // Methods
        protected ActionBase CMSCustomLookUp(string page, LocatorType locatorType, string lookUpField, string input)
        {
            ElementClick(page, locatorType, lookUpField);
            xrmApp.ThinkTime(1000);
            SendKeys(page, locatorType, lookUpField, input);
            WaitElement(locatorType, "//li[contains(@aria-label,'" + input + "')]");
            ElementClick(page, locatorType, "//ul[@tabindex='0']");
            //ElementClick(page, locatorType, "//li[contains(@aria-label,'" + input + "')]");
            return new ActionBase(driver, client, xrmApp);
        }

        protected ActionBase CMSCustomAssignToMe(string page, LocatorType locatorType, string locator)
        {
            xrmApp.ThinkTime(2000);
            ElementClick(page, locatorType, locator);
            xrmApp.Dialogs.Assign(Dialogs.AssignTo.Me);
            //xrmApp.CommandBar.ClickCommand("Refresh");
            return new ActionBase(driver, client, xrmApp);
        }

        protected ActionBase CMSCustomMultiValueOptionSet(string page, LocatorType locatorType, string field, string[] value)
        {
            ElementClick(page, locatorType, "//div[@id='" + field + "_i']");
            ElementClick(page, locatorType, "//input[@id='" + field + "_ledit']//following::button[1]");
            var selectValue = value;
            foreach (var v in selectValue)
            {
                ElementClick(page, locatorType, "//input[@id='" + field + "_ledit']//following::div[text()='" + v + "'][1]");
            }
            //xrmApp.CommandBar.ClickCommand("Refresh");
            return new ActionBase(driver, client, xrmApp);
        }

        protected ActionBase SwitchView(string page, string viewName)
        {
            ElementClick(page, LocatorType.XPath, "//button[contains(@data-id,'ViewSelector')]");
            ElementClick(page, LocatorType.XPath, "//span[text()='" + viewName + "']");
            return new ActionBase(driver, client, xrmApp);
        }

        protected ActionBase Click(string page, LocatorType locatorType, string locator)
        {
            ElementClick(page, locatorType, locator);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase Type(string page, LocatorType locatorType, string locator, string text)
        {
            SendKeys(page, locatorType, locator, text);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase TypeWithTab(string page, LocatorType locatorType, string locator, string text, string tabs)
        {
            SendKeys(page, locatorType, locator, text);
            ElementTabKey(page, locatorType, locator, tabs);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase TypeWithEnter(string page, LocatorType locatorType, string locator, string text)
        {
            SendKeys(page, locatorType, locator, text);
            ElementEnter(page, locatorType, locator);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase TypeWithBackSpace(string page, LocatorType locatorType, string locator, string text, string backSpaces)
        {
            SendKeys(page, locatorType, locator, text);
            ElementBackSpace(page, locatorType, locator, backSpaces);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase DeleteAll(string page, LocatorType locatorType, string locator)
        {
            ElementDeleteAll(page, locatorType, locator);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase DownArrow(string page, LocatorType locatorType, string locator, string repeats)
        {
            ElementDownArrow(page, locatorType, locator, repeats);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase UpArrow(string page, LocatorType locatorType, string locator, string repeats)
        {
            ElementUpArrow(page, locatorType, locator, repeats);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase LeftArrow(string page, LocatorType locatorType, string locator, string repeats)
        {
            ElementLeftArrow(page, locatorType, locator, repeats);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase RightArrow(string page, LocatorType locatorType, string locator, string repeats)
        {
            ElementRightArrow(page, locatorType, locator, repeats);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase ScrollPage(string page, string direction)
        {
            WebScroll(page, direction);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase Clear(string page, LocatorType locatorType, string locator)
        {
            ClearField(page, locatorType, locator);
            return new ActionBase(driver, client, xrmApp); ;
        }

        public ActionBase SelectItem(string page, LocatorType locatorType, string locator, string item)
        {
            ElementSelect(page, locatorType, locator, item);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase Check(string page, LocatorType locatorType, string locator, string option)
        {
            ElementCheck(page, locatorType, locator, option);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase Frame(string page, string frame)
        {
            SwitchFrame(page, frame);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase Window(string page)
        {
            SwitchWindow(page);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase Alert(string page, string response)
        {
            AlertResponse(page, response);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase DragDrop(string page, LocatorType fromLocatorType, string fromLocator, LocatorType toLocatorType, string toLocator)
        {
            DragAndDrop(page, fromLocatorType, fromLocator, toLocatorType, toLocator);
            return new ActionBase(driver, client, xrmApp); ;
        }

        protected ActionBase MouseHover(string page, LocatorType locatorType, string locator)
        {
            MouseHovering(page, locatorType, locator);
            return new ActionBase(driver, client, xrmApp); ;
        }
    }
}