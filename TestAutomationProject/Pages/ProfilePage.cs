﻿using Microsoft.Dynamics365.UIAutomation.Api.UCI;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;

namespace TestAutomationProject.Pages
{
    public class ProfilePage : ActionBase
    {
        // Constructor : Inheritance from PageBase
        public ProfilePage(IWebDriver driver, WebClient client, XrmApp xrmapp) : base(driver, client, xrmapp)
        {
        }

        private readonly string _page = MethodBase.GetCurrentMethod().DeclaringType?.ToString();

        // Methods

        public void NavigateToProfile()
        {
            ElementClick(_page, LocatorType.XPath, "//a[text()='Profile']");
        }

        public void ModifyProfile(string firstName, string lastName, string gender, string age, string address, string phone, string hobby)
        {
            SendKeys(_page, LocatorType.Id, "firstName", firstName);
            SendKeys(_page, LocatorType.Id, "lastName", lastName);
            SendKeys(_page, LocatorType.Id, "gender", gender);
            SendKeys(_page, LocatorType.Id, "age", age);
            SendKeys(_page, LocatorType.Id, "address", address);
            SendKeys(_page, LocatorType.Id, "phone", phone);
            ElementSelect(_page, LocatorType.Id, "hobby", hobby);
        }

        public void ModifyPassword(string currentPassword, string newPassword)
        {
            SendKeys(_page, LocatorType.Id, "currentPassword", currentPassword);
            SendKeys(_page, LocatorType.Id, "newPassword", newPassword);
            SendKeys(_page, LocatorType.Id, "newPasswordConfirmation", newPassword);
        }

        public void ReturnOriginalPassword(string currentPassword, string newPassword)
        {
            SendKeys(_page, LocatorType.Id, "currentPassword", currentPassword);
            SendKeys(_page, LocatorType.Id, "newPassword", newPassword);
            SendKeys(_page, LocatorType.Id, "newPasswordConfirmation", newPassword);
        }

        public void Logout()
        {
            ElementClick(_page, LocatorType.XPath, "//a[text()='Logout']");
        }

        public void VerifyStatus(string expectedStatus)
        {
            ElementClick(_page, LocatorType.XPath, "//button[@type='submit']");
            string actualStatus = GetText(_page, LocatorType.XPath, "//div[@class='result alert alert-success hidden-md-down']").Trim();
            Assert.That(expectedStatus == actualStatus, "expected status: " + expectedStatus + " but was: " + actualStatus);
        }
    }
}